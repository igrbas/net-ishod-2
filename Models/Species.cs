namespace Zoo.Models {

    public class Species {

        public int Id { get; set; }

        public string? Name { get; set; }

        public string? Family { get; set; }

        public string? OriginContinent { get; set; }

        public string? Diet { get; set; }

        public List<Animal>? Animals { get; set; }
    }
}