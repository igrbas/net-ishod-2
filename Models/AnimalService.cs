using System.Collections;
using Microsoft.EntityFrameworkCore;
using Zoo.Data;


namespace Zoo.Models {

    public class AnimalService : IAnimalService {

        public AnimalService(ApplicationDbContext context) {
            Db = context;
        }

        public async Task<IEnumerable> All(string userId) {
            return await Db.Animal.Include(a => a.Species).Where(a => a.IdentityUserId == userId).ToListAsync();
        }

        public async Task<IEnumerable> FilterBySpecies(string userId, string species) {
            return await Db.Animal.Include(a => a.Species).Where(a => a.IdentityUserId == userId && a.Species!.Name == species).ToListAsync();
        }

        public async Task<IEnumerable> FilterByGender(string userId, int gender) {
            return await Db.Animal.Include(a => a.Species).Where(a => a.IdentityUserId == userId && a.Gender == gender).ToListAsync();
        }

        public async Task<Animal?> Get(int id, string userId) {
            return await Db.Animal.Include(a => a.Species).Where(a => a.IdentityUserId == userId).FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task<int> Insert(Animal? animal) {
            if(animal != null) {
                Db.Add(animal);
                return await Db.SaveChangesAsync();
            }
            return -1;
        }

        public async Task<bool> Update(Animal? animal) {
            if(animal != null) {
                try {
                    Db.Update(animal);
                    await Db.SaveChangesAsync();
                    return true;
                } catch (Exception) {
                    return false;
                }
            }
            return false;
        }

        public async Task<int> Delete(int id, string userId) {
            var animal = await Get(id, userId);
            if(animal != null){
                Db.Animal.Remove(animal);
                return await Db.SaveChangesAsync();
            }
            return -1;
        }

        public ApplicationDbContext Db { get; set; }

    }
}