namespace Zoo.Models {

    public class Animal {

        public int Id { get; set; }

        public string? Name { get; set; }

        public DateTime Birthday { get; set; }

        public int Gender { get; set; }

        public int SpeciesId { get; set; }

        public Species? Species { get; set; }

        public string? IdentityUserId { get; set; }

        public IdentityUser? IdentityUser { get; set; }
    }
}