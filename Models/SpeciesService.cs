using System.Collections;
using Microsoft.EntityFrameworkCore;
using Zoo.Data;

namespace Zoo.Models {

    public class SpeciesService : ISpeciesService {

        public SpeciesService(ApplicationDbContext context) {
            Db = context;
        }

        public async Task<IEnumerable> All() {
            return await Db.Species.ToListAsync();
        }

        public async Task<IEnumerable> FilterByFamily(string family) {
            return await Db.Species.Where(s => s.Family == family).ToListAsync();
        }

        public async Task<IEnumerable> FilterByContinent(string continent) {
            return await Db.Species.Where(s => s.OriginContinent == continent).ToListAsync();
        }

        public async Task<Species?> Get(int id) {
            return await Db.Species.FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<int> Insert(Species? species) {
            if(species != null) {
                Db.Add(species);
                return await Db.SaveChangesAsync();
            }
            return -1;
        }

        public async Task<bool> Update(Species? species) {
            if(species != null) {
                try {
                    Db.Update(species);
                    await Db.SaveChangesAsync();
                    return true;
                } catch (Exception) {
                    return false;
                }                
            }
            return false;
        }

        public async Task<int> Delete(int id) {
            var species = await Get(id);
            if(species != null) {
                Db.Remove(species);
                return await Db.SaveChangesAsync();
            }
            return -1;
        }

        public ApplicationDbContext Db { get; set; }
    }
}