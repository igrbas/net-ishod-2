using System.Collections;
using Zoo.Data;

namespace Zoo.Models {

    public interface ISpeciesService {

        Task<IEnumerable> All();

        Task<IEnumerable> FilterByFamily(string family);

        Task<IEnumerable> FilterByContinent(string continent);

        Task<Species?> Get(int id);

        Task<int> Insert(Species? species);

        Task<bool> Update(Species? species);

        Task<int> Delete(int id);

        ApplicationDbContext Db { get; set; }
    }
}