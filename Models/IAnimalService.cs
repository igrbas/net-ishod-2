using System.Collections;
using Zoo.Data;

namespace Zoo.Models {

    public interface IAnimalService {

        Task<IEnumerable> All(string userId);

        Task<IEnumerable> FilterBySpecies(string userId, string species);

        Task<IEnumerable> FilterByGender(string userId, int gender);
        
        Task<Animal?> Get(int id, string userId);

        Task<int> Insert(Animal? animal);

        Task<bool> Update(Animal? animal);

        Task<int> Delete(int id, string userId);

        ApplicationDbContext Db { get; set; }
    }
}