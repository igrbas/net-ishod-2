#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Zoo.Data;
using Zoo.Models;

namespace Zoo.Controllers {

    [Authorize]
    public class SpeciesController : Controller {

        public SpeciesController(ISpeciesService speciesService) {
            this.speciesService = speciesService;
        }

        // GET: Species
        public async Task<IActionResult> Index() {
            return View(await speciesService.All());
        }

        public async Task<IActionResult> FamilyFilter(string family) {
            return View(await speciesService.FilterByFamily(family));
        }

        public async Task<IActionResult> ContinentFilter(string continent) {
            return View(await speciesService.FilterByContinent(continent));
        }

        // GET: Species/Details/5
        public async Task<IActionResult> Details(int? id) {
            if (id == null) return NotFound();

            var species = await speciesService.Get((int) id);

            if (species == null) return NotFound();

            return View(species);
        }

        // GET: Species/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create() {
            return View();
        }

        // POST: Species/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Family,OriginContinent,Diet")] Species species) {
            if (ModelState.IsValid) {
                await speciesService.Insert(species);
                return RedirectToAction(nameof(Index));
            }
            return View(species);
        }

        // GET: Species/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id) {
            if (id == null) return NotFound();

            var species = await speciesService.Get((int) id);

            if (species == null) return NotFound();

            return View(species);
        }

        // POST: Species/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Family,OriginContinent,Diet")] Species species) {
            if (id != species.Id) return NotFound();

            if (ModelState.IsValid) {
                if(await speciesService.Update(species) == true) {
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(species);
        }

        // GET: Species/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id) {
            if (id == null) return NotFound();

            var species = await speciesService.Get((int) id);

            if (species == null) return NotFound();

            return View(species);
        }

        // POST: Species/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id){
            await speciesService.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private readonly ISpeciesService speciesService;
    }
}
