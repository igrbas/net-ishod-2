#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Zoo.Data;
using Zoo.Models;

namespace Zoo.Controllers {
    
    [Authorize]
    public class AnimalsController : Controller {

        public AnimalsController(IAnimalService animalService, UserManager<IdentityUser> userManager) {
            this.animalService = animalService;
            _userManager = userManager;
        }

        // GET: Animals
        public async Task<IActionResult> Index() {
            var user = await _userManager.GetUserAsync(User);
            return View(await animalService.All(user.Id));
        }

        public async Task<IActionResult> SpeciesFilter(string species) {
            var user = await _userManager.GetUserAsync(User); 
            return View(await animalService.FilterBySpecies(user.Id, species));
        }

        public async Task<IActionResult> GenderFilter(int gender) {
            var user = await _userManager.GetUserAsync(User); 
            return View(await animalService.FilterByGender(user.Id, gender));
        }

        // GET: Animals/Details/5
        public async Task<IActionResult> Details(int? id) {
            if (id == null) return NotFound();

            var user = await _userManager.GetUserAsync(User);
            var animal = await animalService.Get((int) id, user.Id);

            if (animal == null) {
                return NotFound();
            }

            return View(animal);
        }

        // GET: Animals/Create
        public IActionResult Create() {
            ViewData["SpeciesId"] = new SelectList(animalService.Db.Species, "Id", "Name");
            return View();
        }

        // POST: Animals/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Birthday,Gender,SpeciesId")] Animal animal) {
            var user = await _userManager.GetUserAsync(User);
            animal.IdentityUserId = user.Id;
            if (ModelState.IsValid) {
                await animalService.Insert(animal);
                return RedirectToAction(nameof(Index));
            }
            ViewData["SpeciesId"] = new SelectList(animalService.Db.Species, "Id", "Name", animal.SpeciesId);
            return View(animal);
        }

        // GET: Animals/Edit/5
        public async Task<IActionResult> Edit(int? id) {
            if (id == null) return NotFound();

            var user = await _userManager.GetUserAsync(User);
            var animal = await animalService.Get((int)id, user.Id);
            if (animal == null) return NotFound();

            ViewData["SpeciesId"] = new SelectList(animalService.Db.Species, "Id", "Name", animal.SpeciesId);
            return View(animal);
        }

        // POST: Animals/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Birthday,Gender,SpeciesId")] Animal animal) {
            var user = await _userManager.GetUserAsync(User);
            animal.IdentityUserId = user.Id;

            if (id != animal.Id) return NotFound();

            if (ModelState.IsValid) {
                if(await animalService.Update(animal) == true) {
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["SpeciesId"] = new SelectList(animalService.Db.Species, "Id", "Name", animal.SpeciesId);
            return View(animal);
        }

        // GET: Animals/Delete/5
        public async Task<IActionResult> Delete(int? id) {
            if (id == null) return NotFound();

            var user = await _userManager.GetUserAsync(User);
            var animal = await animalService.Get((int) id, user.Id);
            if(animal == null) return NotFound();

            return View(animal);
        }

        // POST: Animals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id) {
            var user = await _userManager.GetUserAsync(User);
            await animalService.Delete(id, user.Id);
            return RedirectToAction(nameof(Index));
        }

        private readonly IAnimalService animalService;
        private readonly UserManager<IdentityUser> _userManager;
    }
}
